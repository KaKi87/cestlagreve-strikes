const
    get = require('./get'),
    querystring = require('./querystring'),
    getParams = require('./getParams');

/**
 * @typedef Strike
 * @type Object
 * @param {String} title
 * @param {String} [link]
 */

/**
 * @typedef Day
 * @type {Strike[]}
 */

/**
 * @typedef Month
 * @type {Day[]}
 */

/**
 * @function getStrikes
 * @description Get strikes in a given month, year and optional region or department
 * @param {Number} [month] - MM
 * @param {Number} [year] - YYYY
 * @param {Number} [regionId] - As defined by website
 * @param {Number} [departmentId] - As defined by website
 * @param {Number|String} [departmentCode] - French department code (will make an additional request)
 * @param {Number} [sectorId] - As defined by website
 * @returns {Promise<Month>}
 */
module.exports = async ({
    month = new Date().getMonth() + 1,
    year = new Date().getFullYear(),
    regionId,
    departmentId,
    departmentCode,
    sectorId
} = {}) => {
    if(typeof month !== 'number') throw new TypeError('month : expected number');
    if(!Number.isInteger(month)) throw new TypeError('month : expected integer');
    if(month < 1 || month > 12) throw new RangeError('month : expected between 1 & 12');

    if(typeof year !== 'number') throw new TypeError('year: expected number');
    if(!Number.isInteger(year)) throw new TypeError('year : expected integer');
    if(year.toString().length !== 4) throw new TypeError('year: expected 4 digits');

    const params = {
        'mois': month,
        'annee': year
    };

    if(regionId) params['lieu'] = regionId;
    if(departmentId) params['lieu'] = departmentId;
    if(departmentCode) params['lieu'] = (await getParams()).departments.find(department => department.code === departmentCode).id;
    if(sectorId) params['secteur'] = sectorId;

    const body = await get('https://www.cestlagreve.fr/calendrier/?' + querystring(params));
    const lines = body
        .replace(/&rsquo;/g, '\'')
        .split('\n');
    const days = [];
    for(let i = 0; i < lines.length; i++){
        const line = lines[i].trim();
        const dayMatch = line.match(/^<span class="date" title=".*">([0-9]+)<\/span>$/);
        if(dayMatch){
            const day = parseInt(dayMatch[1]);
            if(day > 1 && !Array.isArray(days[1])) continue;
            if(Array.isArray(days[day])) break;
            days[day] = [];
        }
        const strikesMatch = [...line.matchAll(/<li>(.+?)<\/li>/g)];
        if(strikesMatch){
            for(let j = 0; j < strikesMatch.length; j++){
                const strikeMatch = strikesMatch[j];
                const [title] = strikeMatch.slice(1);
                const day = days.slice(-1)[0];
                if(day) day.push({ title });
            }
        }
    }
    return days;
};