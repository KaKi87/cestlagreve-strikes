const getStrikes = require('./getStrikes');

const twoDigits = n => n.toString().padStart(2, '0');

/**
 * @function getIcal
 * @description Get strikes calendar as iCal
 * @param {Object} options
 * @returns {Promise<string>}
 */
module.exports = async (options = {}) => {
    if(!options.month) options.month = new Date().getMonth() + 1;
    if(!options.year) options.year = new Date().getFullYear();
    const events = [];
    while(true){
        /*** @type Month */
        const monthStrikes = await getStrikes(options);
        if(monthStrikes.slice(1).every(day => !day.length))
            break;
        for(let day = 1; day < monthStrikes.length; day++){
            const strikes = monthStrikes[day];
            for(let i = 0; i < strikes.length; i++){
                const { title, link } = strikes[i];
                const startEvent = events.find(event => event.title === title && event.link === link);
                if(startEvent){
                    startEvent.endYear = options.year;
                    startEvent.endMonth = options.month;
                    startEvent.endDay = day;
                }
                else {
                    events.push({
                        startYear: options.year,
                        endYear: options.year,
                        startMonth: options.month,
                        endMonth: options.month,
                        startDay: day,
                        endDay: day,
                        title,
                        link
                    });
                }
            }
        }
        if(options.month < 12)
            options.month++;
        else {
            options.month = 1;
            options.year++;
        }
    }
    let iCal = 'BEGIN:VCALENDAR\nVERSION:2.0\n';
    for(let i = 0; i < events.length; i++){
        const { startYear, startMonth, startDay, endYear, endMonth, endDay, title, link } = events[i];
        iCal += `BEGIN:VEVENT\nDTSTART;VALUE=DATE:${startYear}${twoDigits(startMonth)}${twoDigits(startDay)}\nDTEND;VALUE=DATE:${endYear}${twoDigits(endMonth)}${twoDigits(endDay)}\nSUMMARY:${title}\n${link ? `DESCRIPTION:${link}\n` : ''}END:VEVENT\n`;
    }
    iCal += 'END:VCALENDAR';
    return iCal;
};
