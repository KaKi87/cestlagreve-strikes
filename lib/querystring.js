/**
 * @function querystring
 * @param {Object} object
 * @returns {string}
 */
module.exports = object => Object.keys(object).map(key => `${key}=${object[key]}`).join('&');