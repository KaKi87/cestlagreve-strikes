const get = require('./get');

module.exports = async () => {
    const body = await get('https://www.cestlagreve.fr/');
    const
        regions = [],
        departments = [],
        sectors = [],
        subSectors = [];
    const lines = body
        .replace(/&nbsp;/g, '')
        .replace(/&rsquo;/g, '\'')
        .split('\n');
    let currentParamType;
    let currentParentParam;
    for(let i = 0; i < lines.length; i++){
        const line = lines[i].trim();
        const selectMatch = line.match(/^<select +name='([a-z]+)'/);
        if(selectMatch) currentParamType = selectMatch[1];
        const optionMatch = line.match(/^<option class="level-([01])" value="([0-9]+)">(.+?)(?: \((.+)\))?<\/option>$/);
        if(optionMatch){
            switch(currentParamType){
                case 'lieu': {
                    const [isRegion, id, name, code] = [!+optionMatch[1], parseInt(optionMatch[2]), optionMatch[3], optionMatch[4]];
                    if(isRegion){
                        const region = { id, name, departments: [] };
                        regions.push(region);
                        currentParentParam = region;
                    }
                    else {
                        const department = { id, name, code: /^[0-9]+$/.test(code) ? parseInt(code) : code };
                        departments.push(department);
                        currentParentParam.departments.push(department);
                    }
                    break;
                }
                case 'secteur': {
                    const [isParent, id, name] = [!+optionMatch[1], parseInt(optionMatch[2]), optionMatch[3]];
                    if(isParent){
                        const sector = { id, name, subSectors: [] };
                        sectors.push(sector);
                        currentParentParam = sector;
                    }
                    else {
                        const subSector = { id, name };
                        subSectors.push(subSector);
                        currentParentParam.subSectors.push(subSector);
                    }
                    break;
                }
            }
        }
    }
    return { regions, departments, sectors, subSectors };
};