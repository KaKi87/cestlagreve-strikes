/**
 * @function get
 * @description Perform HTTPS GET request
 * @param {String} url
 * @returns {Promise<String>}
 */
module.exports = url => new Promise((resolve, reject) => require('https').get(url, res => {
    let body = '';
    res.on('data', chunk => body += chunk);
    res.on('end', () => resolve(body));
    res.on('error', reject);
}));