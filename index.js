module.exports = {
    getParams: require('./lib/getParams'),
    getStrikes: require('./lib/getStrikes'),
    getIcal: require('./lib/getIcal')
};