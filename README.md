# cestlagreve-strikes

Calendar scraper for [cestlagreve.fr](https://www.cestlagreve.fr/), a french website providing all strike dates in France.

## Getting started

### Prerequisites

- NodeJS
- NPM
- Yarn

### Install

From [npm](https://www.npmjs.com/package/cestlagreve-strikes)

`yarn add cestlagreve-strikes`

or

`npm i cestlagreve-strikes --save`

### Use

```js
const cestlagreve = require('cestlagreve-strikes');
```

#### Get parameters

```js
cestlagreve.getParams().then(console.log)
```

**Returns** [this](demo/params.json).

#### Get strikes

```js
cestlagreve.getStrikes({
    month,
    year,
    regionId,
    departmentId,
    departmentCode,
    sectorId
}).then(console.log)
```

**Params :**
- `month` : *(integer, optional)* MM format
<br>Defaults to current month
- `year` : *(integer, optional)* YYYY format
<br>Defaults to current year
- `regionId` : *(integer, optional)* As returned by `getParams()`
- `departmentId` : *(integer, optional)* As returned by `getParams()`
- `departmentCode` : *(string, optional)* French department code
- `sectorId` : *(string, optional)* As returned by `getParams()`

**Note :** defining more than one of the last 3 parameters would be pointless.

**Returns** an array (days of month) of arrays (strikes of day) with the following properties :
- `name` : *(string)* Event name in french
- `link` : *(string, optional)* URL to related blog article in french

#### Get iCal

```js
cestlagreve.getIcal(options).then(console.log)
```

**Params :** same as `getStrikes()`

**Note :** This function will fetch all available future events (including next months).

Returns [this](demo/ical.ics)

## Changelog

* `1.0.0` (2020-03-03) • Initial release
* `1.0.1` (2020-03-04) • Add filter by sector ID
* `1.0.2` (2020-03-04)
	- Fix current month beginning with last days of previous month
	- Add iCal generator
* `1.0.3` (2020-03-04) • Fix iCal date & description tags
* `1.0.4` (2022-09-29) • Handle missing event link